$(document).ready(function(){
    let elem=$('.carousel').carousel();
    let instance = M.Carousel.getInstance(elem);
    $('#tb-1').click(() => {
        instance.next();
        let selectedIndex = $('.carousel-item').filter('.active').index();
        switch (selectedIndex) {
            case 0:
                secondCardClicked();
                break;
            case 1:
                thirdCardClicked();
                break;
            case 2:
                firstCardClicked();
            default:
        }
    });
    $('#tb-2').click(() => {
        instance.prev();
        let selectedIndex = $('.carousel-item').filter('.active').index();
        switch (selectedIndex) {
            case 0:
                thirdCardClicked();
                break;
            case 1:
                firstCardClicked();
                break;
            case 2:
                secondCardClicked();
            default:
        }
    });

    $("#titleHeader2").hide();
    $("#titleHeader3").hide();
    $("#titleText1").hide();
    $('#item1').click(() => {
        firstCardClicked();
    });
    $('#item2').click(() => {
        secondCardClicked();
    });
    $('#item3').click(() => {
        thirdCardClicked();
    });
    function firstCardClicked() {
        $("#titleHeader2").hide();
        $("#titleHeader3").hide();
        $("#titleText1").hide();
        $("#titleText2").show();
        $("#titleText3").show();
        $("#titleHeader1").show();
    }
    function secondCardClicked(){
        $("#titleHeader1").hide();
        $("#titleHeader3").hide();
        $("#titleText2").hide();
        $("#titleText1").show();
        $("#titleText3").show();
        $("#titleHeader2").show();
    }
    function thirdCardClicked(){
        $("#titleHeader1").hide();
        $("#titleHeader2").hide();
        $("#titleText3").hide();
        $("#titleText1").show();
        $("#titleText2").show();
        $("#titleHeader3").show();
    }
})